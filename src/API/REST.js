const HOST = 'http://localhost:3010/'

export default class REST {
  constructor(url) {
    this.url = url
    return this
  }

  read = (id) =>
    id
      ? fetchJSON(`${this.url}/${id}`, { method: 'GET' })
      : fetchJSON(this.url, { method: 'GET' })

  create = data =>
    fetchJSON(
      this.url,
      {
        method: 'POST',
        body  : JSON.stringify(data)
      })

  update = (data) =>
    fetchJSON(
      this.url + `/${data.id}`,
      {
        method: 'PUT',
        body  : JSON.stringify(data)
      })

  delete = ({ id }) =>
    fetchJSON(
      this.url + `/${id}`,
      { method: 'DELETE' })
}

export const fetchJSON = (url, options = {}) =>
  new Promise((resolve, reject) => {
    return fetch(HOST + url, {
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
        Accept        : 'application/json',
      },
      ...options
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error))
  })
