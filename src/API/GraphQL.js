import gql from 'graphql-tag'

export const query =
{
  get: {
    tasks:
    gql`query tasks($lastCreatedAt: Float = 0, $limit: Int = 0) {
        tasks(lastCreatedAt: $lastCreatedAt, limit: $limit)
        { id lat lng address service task description createdAt }
    }`,
    task :
    gql`query task($id: String!) {
        task(id: $id)
        { id lat lng address service task description createdAt }
    }`,
    user :
    gql`query user($id: String!) {
        user(id: $id)
        { id name email }
    }`,
  }
}

export const mutation =
{
  create: {
    task:
    gql`mutation createTask($input:CreateTaskInput!) {
        createTask(input:$input)
        { id lat lng address service task description createdAt }
    }`,
  },

  delete: {
    task:
    gql`mutation removeTask($id: String!) {
        removeTask(id:$id)
    }`
  },

  update: {
    task:
    gql`mutation updateTask($id: String!,$input:UpdateTaskInput!) {
        updateTask(id:$id, input:$input)
        { id lat lng address service task description createdAt }
    }`,
  },
}
