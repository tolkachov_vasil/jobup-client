import { GOOGLE_MAP_API_KEY } from '../config/strings'

const GM_API_URL = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='

export const getAddressByLocation = ({ lat, lng }) => {
  const url = `${GM_API_URL}${lat},${lng}&key=${GOOGLE_MAP_API_KEY}`

  return new Promise((resolve, reject) => {
    return fetch(url, {
      method     : 'GET',
      credentials: 'omit',
      headers    : { Accept: 'application/json' }
    })
      .then(response => (response.status !== 200 ? reject(response) : response))
      .then(response => response.json())
      .then(response => resolve(response))
      .catch(error => reject(error))
  })
}
