import REST from './REST'

const routes = {
  logout: new REST('logout'),
  login : new REST('login'),
}
export default routes
