import 'normalize.css'
import 'open-sans-all'
import createBrowserHistory from 'history/createBrowserHistory'
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import apollo from './config/apollo'
import { configureStore } from './helpers/store'
import Layout from './modules/root/Layout'

const history = createBrowserHistory()
const store = configureStore({ apollo, history })

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Layout />
        </Router>
      </Provider>
    )
  }
}

export default App
