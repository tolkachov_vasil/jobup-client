import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button } from '../../common'
import { ActionType, UserType } from '../../helpers/prop-types'
import { logoutRequest } from './store/actions'

class Layout extends Component {
  static propTypes = {
    user         : UserType,
    logoutRequest: ActionType,
  }

  render() {
    const { user, logoutRequest } = this.props

    return (
      user && user.name ?
      <div>
        {user.name}
        <Button
          caption={'LOGOUT'}
          mode='primary'
          onPress={() => logoutRequest()}
        />
      </div>
        :
      null
    )
  }
}

const withStore = connect(s =>
    ({
      user: s.cache.user
    }),
  {
    logoutRequest,
  })(Layout)

export default withStore
