import { all, call, takeEvery, put } from 'redux-saga/effects'
import routes from '../../../API/routes'
import { AUTH_TOKEN_KEY, USER_ID_KEY } from '../../../config/strings'
import { magicUpdate } from '../../../lib/magicGQL'
import { PROFILE__LOGIN_REQUEST, PROFILE__LOGOUT_REQUEST } from './actions'

function* logout({ next }) {
  localStorage.clear()
  yield put(magicUpdate('set', 'user', null))
  yield put(magicUpdate('set', 'task', null))
  yield put(magicUpdate('set', 'tasks', []))
  yield next && next()
}

function* login({ data, next }) {
  try {
    const { token = '', userId = '' } = yield call(routes.login.create, data)
    localStorage.setItem(AUTH_TOKEN_KEY, token)
    localStorage.setItem(USER_ID_KEY, userId)
    yield put(magicUpdate('merge', 'user', { id: userId }))
    yield next && next()
  }
  catch (e) {
    console.log('error', e)
  }
}

export function* saga() {
  yield all([
    takeEvery(PROFILE__LOGOUT_REQUEST, logout),
    takeEvery(PROFILE__LOGIN_REQUEST, login),
  ])
}
