import { createAction } from '../../../helpers/actions'

export const PROFILE__LOGOUT_REQUEST = 'PROFILE__LOGOUT_REQUEST'
export const PROFILE__LOGIN_REQUEST = 'PROFILE__LOGIN_REQUEST'

export const logoutRequest = createAction(PROFILE__LOGOUT_REQUEST, 'next')
export const loginRequest = createAction(PROFILE__LOGIN_REQUEST, 'data', 'next')
