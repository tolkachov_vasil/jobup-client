import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavLink, withRouter } from 'react-router-dom'
import { Button, Text, TextInput, Row, Col } from '../../common'
import ServiceIcon from '../../common/SVG/ServiceIcon'
import { color, screen, size, unselectable } from '../../config/styles'
import { ActionType, IdType, TaskType } from '../../helpers/prop-types'
import { taskDTO, taskTitle } from '../../helpers/task'
import { magicRequest, magicUpdate } from '../../lib/magicGQL'

const initialTask = {
  address    : null,
  service    : '',
  task       : '',
  lat        : 0,
  lng        : 0,
  description: '',
}

class TaskForm extends Component {
  static propTypes = {
    id          : IdType,
    task        : TaskType,
    taskOptions : PropTypes.object,
    magicRequest: ActionType,
    magicUpdate : ActionType,
  }

  componentWillMount() {
    const { magicRequest, magicUpdate, id } = this.props

    id
      ? magicRequest('get', 'task', { id })
      : magicUpdate('set', 'task', initialTask)
  }

  componentWillUnmount() {
    const { magicUpdate } = this.props
    magicUpdate('set', 'task', null)
  }

  get isNew() {
    return !this.props.id
  }

  get isValid() {
    const { task: { service, task, lat, lng } } = this.props

    return !!lat &&
           !!lng &&
           !!service.trim() &&
           !!task.trim()
  }

  update = (field, value) => {
    const { magicUpdate } = this.props
    magicUpdate('merge', 'task', { [field]: value })
  }

  submit = () => {
    const {
      task,
      history,
      magicRequest,
    } = this.props

    const DTO = taskDTO(task)

    this.isNew
      ? magicRequest('create', 'task', { input: { ...DTO } })
      : magicRequest('update', 'task', { id: task.id, input: { ...DTO } })

    history.goBack()
  }

  render() {
    const { task: value } = this.props

    if (!value) {
      return null
    }
    const { service, task, address, description } = value

    return (
      <div className={css(styles.TaskForm)}>

        <div className={css(styles.title, styles.section)}>
          <Text
            mode='block spaced gapBottom gray'
            value={this.isNew ? 'NEW TASK ' : 'EDIT TASK '}
          />
          <Text
            mode='block largest gapBottom'
            value={taskTitle(value)}
          />
          {address &&
           <Text
             mode='block smaller gapBottom'
             value={`My address is ${address}`}
           />}
          <Button
            disabled={!this.isValid}
            caption={this.isNew ? 'CREATE' : 'UPDATE'}
            mode='primary larger'
            onPress={this.submit}
          />
          <NavLink to='/'>
            <Button caption='CANCEL' mode='larger' />
          </NavLink>
        </div>

        <div className={css(styles.section)}>
          <Text mode='block spaced gapBottom gray' value='LOCATION' />
          <Text mode='block' value={address} />
        </div>

        <div className={css(styles.section)}>
          <Text mode='block spaced gapBottom gray' value='SERVICE TYPE' />
          {this.renderServices(service)}
        </div>

        {this.renderTasks(service, task)}

        <div className={css(styles.section)}>
          <Text mode='block spaced gapBottom gray' value='TASK DESCRIPTION' />
          <TextInput
            value={description}
            type='textarea'
            rows={4}
            onChange={v => this.update('description', v)}
          />
        </div>

      </div>
    )
  }

  renderServices(service) {
    const { taskOptions } = this.props

    return <Row>
      {Object.keys(taskOptions)
        .map(name =>
          <Col key={name}>
            <div
              className={css(styles.serviceType)}
              onClick={() => this.update('service', name) &&
                             this.update('task', '')}
            >
              <ServiceIcon
                mode={name === service ? 'active' : ''}
                name={name}
              />
              <Text
                className={css(styles.serviceTypeText)}
                mode='block small center gapTop'
                value={name}
              />
            </div>
          </Col>
        )}
    </Row>
  }

  renderTasks(service, task) {
    const { taskOptions } = this.props

    return (
      service &&
      <div className={css(styles.section)}>
        <Text
          mode='block spaced gapBottom gray'
          value={`${service.toUpperCase()} TASKS`}
        />
        <Row mode='wrap start'>
          {taskOptions[service]
            .map(name =>
              <Col key={name} mode='center'>
                <div
                  className={css(
                    name === task
                      ? [styles.task, styles.task_active]
                      : styles.task
                  )}
                  onClick={() => this.update('task', name)}
                >
                  <Text mode='block' value={name} />
                </div>
              </Col>
            )}
        </Row>
      </div>
    )
  }
}

export default withRouter(
  connect(
    s => ({
      task       : s.cache.task,
      taskOptions: s.dashboard.taskOptions,
    }),
    {
      magicRequest,
      magicUpdate,
    }
  )(TaskForm))

const styles = StyleSheet.create({
  TaskForm: {
    width         : 500,
    background    : color.panelBg,
    position      : 'fixed',
    overflowY     : 'auto',
    right         : 0,
    top           : size.headerHeight,
    bottom        : 0,
    transition    : '1s',
    maxWidth      : '100%',
    [screen.phone]: {
      top : size.headerHeightPhone,
      left: 0
    },
  },

  _hidden: {
    width: 0,
  },

  title: {
    background: '#F1F1F1',
  },

  section: {
    padding     : '20px 40px',
    borderBottom: '1px solid #F1F1F1',
  },

  serviceType: {
    cursor: 'pointer',
  },

  serviceTypeText: {
    [screen.phone]: {
      height: 0,
      color : 'transparent',
    }
  },

  task: {
    ...unselectable,
    padding     : '14px 18px',
    cursor      : 'pointer',
    border      : '1px solid',
    borderRadius: 100,
    borderColor : 'transparent',
  },

  task_active: {
    borderColor: color.active,
  },
})
