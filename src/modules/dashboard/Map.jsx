import { css, StyleSheet } from 'aphrodite/no-important'
import GoogleMapReact from 'google-map-react'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Marker } from '../../common/SVG/Marker'
import { GOOGLE_MAP_API_KEY } from '../../config/strings'
import { screen, size } from '../../config/styles'
import { LocationType, TaskType } from '../../helpers/prop-types'

class Map extends Component {
  static propTypes = {
    center          : LocationType,
    tasks           : PropTypes.arrayOf(TaskType),
    zoom            : PropTypes.number,
    task            : TaskType,
    onSelectLocation: PropTypes.func.isRequired,
  }

  static defaultProps = { zoom: 14 }

  render() {
    const {
      center,
      tasks,
      task,
      onSelectLocation,
      zoom
    } = this.props

    return (
      <div className={css(styles.Map)}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: GOOGLE_MAP_API_KEY }}
          defaultZoom={zoom}
          center={center}
          onClick={({ lat, lng }) => onSelectLocation({ lat, lng })}
        >

          {task &&
           <Marker {...task} />}

          {tasks.map(t =>
            <Marker {...t} key={t.id} inactive />)}

        </GoogleMapReact>
      </div>
    )
  }
}

export default Map

const styles = StyleSheet.create({
  Map: {
    position      : 'fixed',
    top           : size.headerHeight,
    bottom        : 0,
    left          : 0,
    right         : 0,
    [screen.phone]: {
      top: size.headerHeightPhone,
    },
  },
})
