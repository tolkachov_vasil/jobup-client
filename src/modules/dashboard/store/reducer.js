import {
  DASHBOARD__UPDATE_MAP_CENTER
} from './actions'

const initialState = {
  mapCenter  : { lat: 59.955413, lng: 30.337844 },
  taskOptions: {
    Electrician: [
      'Replace a bulb',
      'Fix a cable',
      'Repair a TV',
      'Repair a wash',
      'Repair a oven',
    ],
    Plumber    : [
      'Unblock a toilet',
      'Unblock a sink',
      'Fix a water leak',
      'Install a sink',
      'Install a shower',
      'Install a toilet',
    ],
    Gardener   : [
      'Other',
    ],
    Housekeeper: [
      'Other',
    ],
    Cook       : [
      'Make a toast',
      'Make a pizza',
      'Make a cake',
      'Make a coffey',
      'Make a tea',
    ],
  }
}

export default function (state = initialState, action) {
  switch(action.type) {

  case DASHBOARD__UPDATE_MAP_CENTER:
    return {
      ...state,
      mapCenter: action.location
    }

  default:
    return state
  }
}
