import { delay } from 'redux-saga'
import { all, call, put, takeEvery } from 'redux-saga/effects'
import { getAddressByLocation } from '../../../API/google-map'
import { getMyLocation } from '../../../helpers/map'
import { magicUpdate } from '../../../lib/magicGQL'
import {
  DASHBOARD__DETECT_LOCATION,
  DASHBOARD__GET_ADDRESS_REQUEST,
  updateMapCenter,
} from './actions'

function* getAddress(context, { location }) {
  console.log('context', context)
  const { error, results } = yield call(getAddressByLocation, location)
  if (error) {
    console.log('error', error)
  } else {
    if (results[0]) {
      const address = results[0] ? results[0].formatted_address : null
      const { lat, lng } = location

      yield put(magicUpdate('merge', 'task', { lat, lng, address }))
    }
  }
}

function* detectLocation() {
  const location = yield call(getMyLocation)
  yield call(delay, 500)
  yield put(updateMapCenter(location))
}

export function* saga(context) {
  yield all([
    takeEvery(DASHBOARD__GET_ADDRESS_REQUEST, getAddress, context),
    takeEvery(DASHBOARD__DETECT_LOCATION, detectLocation),
  ])
}
