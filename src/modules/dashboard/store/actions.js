import { createAction } from '../../../helpers/actions'

export const DASHBOARD__UPDATE_MAP_CENTER = 'DASHBOARD__UPDATE_MAP_CENTER'
export const DASHBOARD__DETECT_LOCATION = 'DASHBOARD__DETECT_LOCATION'
export const DASHBOARD__GET_ADDRESS_REQUEST = 'DASHBOARD__GET_ADDRESS_REQUEST'
export const DASHBOARD__UPDATE_ADDRESS = 'DASHBOARD__UPDATE_ADDRESS'

export const updateMapCenter = createAction(DASHBOARD__UPDATE_MAP_CENTER, 'location')
export const detectLocation = createAction(DASHBOARD__DETECT_LOCATION)
export const getAddressRequest = createAction(DASHBOARD__GET_ADDRESS_REQUEST, 'location')
export const updateAddress = createAction(DASHBOARD__UPDATE_ADDRESS, 'address')
