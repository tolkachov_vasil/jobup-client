import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Button, Col, Row, Text } from '../../common'
import { color, panel, touchable } from '../../config/styles'
import { formattedDate } from '../../helpers/date'
import { TaskType } from '../../helpers/prop-types'
import { taskTitle } from '../../helpers/task'

class Task extends Component {
  static propTypes = {
    value   : TaskType.isRequired,
    onSelect: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    active  : PropTypes.bool,
  }

  render() {
    const { onDelete, value, onSelect, active } = this.props

    return (
      <div
        onClick={() => onSelect(value)}
        className={css(styles.Task, active && styles._active)}
      >
        <Text
          mode='block gray gapBottom'
          value={formattedDate(value.createdAt)}
        />
        <Text
          mode='block larger gapBottom'
          value={taskTitle(value)}
        />
        <Row mode='start'>
          <Col>
            <Link to={`/edit/${value.id}`}>
              <Button
                mode='primary'
                caption='EDIT'
              />
            </Link>
          </Col>
          <Col>
            <Button
              onPress={() => onDelete(value)}
              caption='DELETE'
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default Task

const styles = StyleSheet.create({
  Task: {
    ...panel,
    ...touchable,
    padding  : '20px 20px 10px 20px',
    marginTop: 10,
  },

  _active: {
    backgroundColor: color.activeTask,
  }
})
