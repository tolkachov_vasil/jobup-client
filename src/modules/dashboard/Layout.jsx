import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ConfirmationDialog } from '../../common'
import { ActionType, LocationType, TaskType } from '../../helpers/prop-types'
import { magicRequest } from '../../lib/magicGQL'
import { closeModal, showModal } from '../root/store/actions'
import Map from './Map'
import Tasks from './Tasks'
import {
  getAddressRequest,
  updateAddress,
  updateMapCenter,
} from './store/actions'

class Layout extends Component {
  static propTypes = {
    task             : TaskType,
    tasks            : PropTypes.arrayOf(TaskType),
    mapCenter        : LocationType,
    getAddressRequest: ActionType,
    updateAddress    : ActionType,
    updateMapCenter  : ActionType,
    magicRequest     : ActionType,
    showModal        : ActionType,
    closeModal       : ActionType,
  }

  updateSelectedPosition = location => {
    const { getAddressRequest, task } = this.props
    task && getAddressRequest(location)
  }

  deleteTask = task => {
    const { showModal, closeModal, magicRequest } = this.props

    showModal(
      <ConfirmationDialog
        onYes={() => {
          closeModal()
          magicRequest('delete', 'task', { id: task.id })
        }}
      />
    )
  }

  selectTask = task => {
    const { updateMapCenter } = this.props
    updateMapCenter(task)
  }

  render() {
    const {
      task,
      tasks,
      mapCenter,
    } = this.props

    return (
      <div>
        <Map
          task={task}
          tasks={tasks}
          center={mapCenter}
          onSelectLocation={this.updateSelectedPosition}
        />
        <Tasks
          items={tasks}
          activeItem={task}
          onSelect={this.selectTask}
          onDelete={this.deleteTask}
        />
      </div>
    )
  }
}

export default connect(
  s => ({
    task     : s.cache.task,
    tasks    : s.cache.tasks,
    mapCenter: s.dashboard.mapCenter,
  }), {
    getAddressRequest,
    updateAddress,
    updateMapCenter,
    magicRequest,
    showModal,
    closeModal,
  })(Layout)
