import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import Button from '../../common/Button'
import { panel, touchable, screen } from '../../config/styles'
import { TaskType } from '../../helpers/prop-types'
import Task from './Task'

class Tasks extends Component {
  static propTypes = {
    items     : PropTypes.arrayOf(TaskType),
    onSelect  : PropTypes.func.isRequired,
    onDelete  : PropTypes.func.isRequired,
    activeItem: TaskType,
  }

  get sorted() {
    const { items } = this.props
    return [...items].sort((a, b) => a.createdAt - b.createdAt)
  }

  render() {
    const {
      onSelect,
      onDelete,
      activeItem,
    } = this.props

    return (
      <div className={css(styles.Tasks)}>
        <div className={css(styles.NewTask)}>
          <NavLink to='/new'>
            <Button caption='+ NEW TASK' mode='expanded' />
          </NavLink>
        </div>
        {this.sorted
          .map(task =>
            <Task
              key={task.id}
              value={task}
              active={activeItem && task.id === activeItem.id}
              onSelect={onSelect}
              onDelete={onDelete}
            />)}
      </div>
    )
  }
}

export default Tasks

const styles = StyleSheet.create({
  Tasks: {
    position      : 'absolute',
    marginLeft    : '5%',
    marginBottom  : 10,
    width         : 330,
    [screen.phone]: {
      marginLeft : 5,
      marginRight: 5,
      width      : 'calc(100% - 10px)',
    },
  },

  NewTask: {
    ...panel,
    ...touchable,
    padding   : '10px 0',
    marginTop : 10,
    textAlign : 'center',
    transition: '0.2s',
  },
})
