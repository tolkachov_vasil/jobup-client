import { createAction } from '../../../helpers/actions'

export const HISTORY__ADD_EVENT = 'HISTORY__ADD_EVENT'

export const addEvent = createAction(HISTORY__ADD_EVENT, 'text')

