import { HISTORY__ADD_EVENT } from './actions'

const initialState = {
  events: []
}

export default function (state = initialState, action) {
  switch(action.type) {
  case HISTORY__ADD_EVENT:
    return {
      ...state,
      events: [...state.events, action.text]
    }
  default:
    return state
  }
}
