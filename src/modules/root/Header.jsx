import { css, StyleSheet } from 'aphrodite/no-important'
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { font, screen, size } from '../../config/styles'
import { Row, Col } from '../../common'

const isDashboardActive = (match, location) => match || ['new', 'edit'].includes(location.pathname.split('/')[1])

class Header extends Component {
  render() {
    return (
      <div className={css(styles.Header)}>
        <Row>
          <Col>
            <div className={css(styles.logo)}> jobUp</div>
          </Col>
          <Col> {this.renderLink('DASHBOARD', '/', isDashboardActive)} </Col>
          <Col> {this.renderLink('HISTORY', '/history')} </Col>
          <Col mode='larger right'> {this.renderLink('PROFILE', '/profile')} </Col>
        </Row>
      </div>
    )
  }

  renderLink = (title, url, isActive = null) =>
    <NavLink
      to={url}
      exact
      isActive={isActive}
      className={css(styles.link)}
      activeClassName={css(styles.link_active)}
    >
      {title}
    </NavLink>
}

export default Header

const styles = StyleSheet.create({
  Header: {
    width         : '90%',
    margin        : '0 auto',
    background    : 'white',
    padding       : '0 5% 0 5%',
    position      : 'fixed',
    zIndex        : 100,
    top           : 0,
    height        : size.headerHeight,
    [screen.phone]: {
      height: size.headerHeightPhone,
    }
  },

  logo: {
    ...font({ size: 20 }),
    color  : '#FA4C6F',
    padding: '7px 0',
  },

  link: {
    ...font({ size: 16 }),
    display       : 'inline-block',
    textDecoration: 'none',
    color         : 'black',
    padding       : '7px 0',
    borderTop     : '4px solid',
    borderColor   : 'transparent',
  },

  link_active: {
    borderColor   : 'black',
    [screen.phone]: {
      border    : 0,
      fontWeight: 'bold',
    },
  },
})
