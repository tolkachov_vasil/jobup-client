import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { zIndex } from '../../config/styles'
import { ActionType } from '../../helpers/prop-types'
import { closeModal } from './store/actions'

class Modal extends Component {
  static propTypes = {
    modal     : PropTypes.object,
    closeModal: ActionType,
  }

  render() {
    const {
      modal: { content, active, noCancellable },
      closeModal,
    } = this.props

    return (
      active &&
      <div
        className={css(styles.Modal__bg)}
        onClick={() => !noCancellable && closeModal()}
      >
        <div
          className={css(styles.Modal__content)}
          onClick={e => e.stopPropagation()}
        >
          {content}
        </div>
      </div>
    )
  }
}

export default connect(
  s => ({
    modal: s.root.modal
  }), {
    closeModal
  })(Modal)

const styles = StyleSheet.create({
  Modal__bg: {
    zIndex        : zIndex.modalBg,
    background    : 'rgba(0,0,0,0.6)',
    position      : 'fixed',
    top           : 0,
    bottom        : 0,
    left          : 0,
    right         : 0,
    display       : 'flex',
    flexDirection : 'column',
    justifyContent: 'center',
    alignItems    : 'center',
  },

  Modal__content: {
    zIndex: zIndex.modalContent,
  }
})

