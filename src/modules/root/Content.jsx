import { css, StyleSheet } from 'aphrodite/no-important'
import React, { Component } from 'react'
import { screen, size } from '../../config/styles'

class Content extends Component {
  render() {
    return (
      <div className={css(styles.Content)}>
        {this.props.children}
      </div>
    )
  }
}

export default Content

const styles = StyleSheet.create({
  Content: {
    marginTop     : size.headerHeight,
    [screen.phone]: {
      marginTop: size.headerHeightPhone,
    },
  }
})
