import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Button, Text, TextInput } from '../../common/index'
import { panel } from '../../config/styles'

class LoginForm extends Component {
  static propTypes = {
    onLogin: PropTypes.func.isRequired,
    //onRegister: PropTypes.func.isRequired,
  }

  state = {
    email   : '',
    password: '',
  }

  update = obj =>
    this.setState({ ...obj })

  render() {
    const { onLogin } = this.props
    const value = this.state

    return (
      <div className={css(styles.LoginForm)}>
        <Text value='EMAIL' mode='block spaced gray' />
        <TextInput
          value={value.title}
          name='email'
          type='email'
          mode='block'
          onChange={this.update}
        />
        <Text value='PASSWORD' mode='block spaced gray' />
        <TextInput
          value={value.title}
          name='password'
          type='password'
          mode='block'
          onChange={this.update}
        />
        <Button
          caption='LOGIN'
          mode='primary larger'
          onPress={() => onLogin(value)}
        />
        <Button
          mode='larger'
          caption='REGISTER'
        />
      </div>
    )
  }
}

export default LoginForm

const styles = StyleSheet.create({
  LoginForm: {
    ...panel,
    padding: 20,
  },
})
