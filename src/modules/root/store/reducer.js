import {
  SHOW_MODAL,
  CLOSE_MODAL,
  CONFIRM_ACTION,
  DISMISS_ACTION,
} from './actions'

const initialState = {
  errors : {},
  flashes: [],
  modal  : {
    active : false,
    content: null,
  },
  spinner: null,
}

export default function (state = initialState, action) {
  switch(action.type) {

  case SHOW_MODAL:
    return {
      ...state,
      modal: {
        active       : true,
        content      : action.content,
        noCancellable: action.noCancellable
      }
    }

  case CONFIRM_ACTION:
  case DISMISS_ACTION:
  case CLOSE_MODAL:
    return {
      ...state,
      modal: { active: false, content: null }
    }

  default:
    return state
  }
}
