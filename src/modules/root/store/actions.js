import { createAction } from '../../../helpers/actions'

export const SHOW_MODAL = 'SHOW_MODAL'
export const CLOSE_MODAL = 'CLOSE_MODAL'
export const CONFIRM_ACTION = 'CONFIRM_ACTION'
export const DISMISS_ACTION = 'DISMISS_ACTION'

export const showModal = createAction(SHOW_MODAL, 'content','noCancellable')
export const closeModal = createAction(CLOSE_MODAL)
export const confirmAction = createAction(CONFIRM_ACTION)
export const dismissAction = createAction(DISMISS_ACTION)
