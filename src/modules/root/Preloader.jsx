import * as React from 'react'
import { Component } from 'react'
import { connect } from 'react-redux'
import { ActionType, UserType } from '../../helpers/prop-types'
import { magicRequest } from '../../lib/magicGQL'
import { detectLocation } from '../dashboard/store/actions'
import LoginForm from './LoginForm'
import { loginRequest } from '../profile/store/actions'
import { closeModal, showModal } from './store/actions'

class Preloader extends Component {
  static propTypes = {
    user          : UserType,
    detectLocation: ActionType,
    magicRequest  : ActionType,
    showModal     : ActionType,
    closeModal    : ActionType,
    loginRequest  : ActionType,
  }

  componentWillMount() {
    this.checkUser()
  }

  componentWillReceiveProps({ user }) {
    if (!user) {
      this.login()
    } else {
      const { user: oldUser } = this.props

      if (!oldUser || user.id !== oldUser.id) {
        this.load(user.id)
      }
    }
  }

  checkUser() {
    const { user } = this.props
    user && user.id ? this.load(user.id) : this.login()
  }

  load(id) {
    const { detectLocation, magicRequest } = this.props

    magicRequest('get', 'user', { id })
    magicRequest('get', 'tasks')
    detectLocation()
  }

  login() {
    const { showModal, loginRequest, closeModal } = this.props

    showModal(
      <LoginForm onLogin={data => loginRequest(data, closeModal)} />,
      true
    )
  }

  render = () => null
}

export default connect(
  s => ({
    user: s.cache.user,
  }),
  {
    detectLocation,
    showModal,
    closeModal,
    loginRequest,
    magicRequest,
  }
)(Preloader)
