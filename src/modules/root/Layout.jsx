import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch } from 'react-router-dom'
import { Modal } from '../../common'
import WithRouter from '../../common/wrappers/WithRouter'
import { TaskType, UserType } from '../../helpers/prop-types'
import { default as DashboardLayout } from '../dashboard/Layout'
import TaskForm from '../dashboard/TaskForm'
import { default as HistoryLayout } from '../history/Layout'
import { default as ProfileLayout } from '../profile/Layout'
import Content from './Content'
import Header from './Header'
import Preloader from './Preloader'

class Layout extends Component {
  static propTypes = {
    user : UserType,
    tasks: PropTypes.arrayOf(TaskType),
  }

  render() {
    const { user, tasks } = this.props

    return (
      <div>
        <Preloader />
        <Header user={user} />
        <Content>
          <Switch>

            <Route exact path='/history' component={HistoryLayout} />

            <Route exact path='/profile'>
              <ProfileLayout user={user} />
            </Route>

            <Route path='/'>
              <div>
                <DashboardLayout tasks={tasks} user={user} />

                <Switch>
                  <Route
                    path='/edit/:id'
                    component={({ match }) =>
                      <TaskForm id={match.params.id} />}
                  />
                  <Route path='/new' component={TaskForm} />
                </Switch>

              </div>
            </Route>

          </Switch>
        </Content>
        <Modal />
      </div>
    )
  }
}

export default WithRouter(
  connect(
    s => ({
      tasks: s.cache.tasks,
      user : s.cache.user,
    })
  )(Layout))
