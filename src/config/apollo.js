import { InMemoryCache } from 'apollo-cache-inmemory'
import ApolloClient from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { onError } from 'apollo-link-error'
import { HttpLink } from 'apollo-link-http'
import { AUTH_TOKEN_KEY, GRAPHQL_ENDPOINT_URL } from './strings'

const token = localStorage.getItem(AUTH_TOKEN_KEY)

const httpLink = new HttpLink({ uri: GRAPHQL_ENDPOINT_URL })

const authMiddleware = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: token ? `JWT ${token}` : null
    }
  })
  return forward(operation)
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) =>
      console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`)
    )
  }
  if (networkError) {
    console.log(`[Network error]: ${networkError}`)
  }
})

const cache = new InMemoryCache({
  dataIdFromObject: o => o.id,
  addTypename     : true,
})

const apollo = new ApolloClient({
  link : ApolloLink.from([errorLink, authMiddleware, httpLink]),
  cache: cache.restore(window.__APOLLO_CLIENT__),
})

export default apollo
