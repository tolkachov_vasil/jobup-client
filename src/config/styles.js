export const screen = {
  phone         : '@media only screen and (max-width: 479px)',
  phoneLandscape: '@media only screen and (min-width: 480px) and (max-width: 767px)',
  tablet        : '@media only screen and (min-width: 768px) and (max-width: 991px)',
  desktop       : '@media only screen and (min-width: 992px)',
}

export const font = (options = {}) => {
  const { style = 'normal', weight = 'normal', size = '14px', family = 'Open Sans' } = options
  return {
    fontFamily: family,
    fontStyle : style,
    fontWeight: weight,
    fontSize  : size,
  }
}

export const unselectable = {
  '-moz-user-select'   : 'none',
  '-webkit-user-select': 'none',
  '-ms-user-select'    : 'none',
  'user-select'        : 'none',
}

export const touchable = {
  ...unselectable,
  ':active' : { transform: 'scale(0.95, 0.95)' },
  cursor    : 'pointer',
  transition: '0.2s',
}

export const size = {
  headerHeight     : '45px',
  headerHeightPhone: '140px',
  marker           : '30px',
  font             : {
    smallest: '10px',
    small   : '12px',
    smaller : '13px',
    default : '14px',
    larger  : '16px',
    large   : '18px',
    largest : '20px',
  }
}

export const zIndex = {
  modalBg     : 1000,
  modalContent: 1001,
}

export const color = {
  active    : '#4c70fe',
  activeTask: '#d3dcff',
  panelBg   : '#fff',
  gray      : '#909090',
}

export const panel = {
  background  : '#FFFFFF',
  boxShadow   : '0 1px 2px 0 rgba(0,0,0,0.30)',
  borderRadius: '4px',
}
