import { USER_ID_KEY } from '../config/strings'
import { createMagicReducer } from '../lib/magicGQL'
import { default as dashboard } from '../modules/dashboard/store/reducer'
import { default as history } from '../modules/history/store/reducer'
import { default as profile } from '../modules/profile/store/reducer'
import { default as root } from '../modules/root/store/reducer'

const cache = createMagicReducer({
  tasks: [],
  task : null,
  user : {
    id: localStorage.getItem(USER_ID_KEY)
  },
})

export default {
  root,
  dashboard,
  history,
  profile,
  cache,
}
