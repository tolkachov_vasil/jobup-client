import { all } from 'redux-saga/effects'
import { mutation, query } from '../API/GraphQL'
import { magicSaga } from '../lib/magicGQL'
import { saga as dashboard } from '../modules/dashboard/store/saga'
import { saga as profile } from '../modules/profile/store/saga'
import { saga as root } from '../modules/root/store/saga'

export default function* rootSaga(context) {
  yield all([
    dashboard(context),
    profile(context),
    root(context),
    magicSaga(context, { query, mutation }),
  ])
}
