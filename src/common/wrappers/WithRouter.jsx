import PropTypes from 'prop-types'
import React, { Component } from 'react'

const WithRouter = WrappedComponent => {
  return class WithRouterComponent extends Component {
    static contextTypes = {
      router: PropTypes.object
    }

    render() {
      const { router } = this.context
      return <WrappedComponent {...this.props} router={router} />
    }
  }
}

export default WithRouter
