import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { font } from '../config/styles'

class TextInput extends Component {
  static propTypes = {
    mode    : PropTypes.string,
    name    : PropTypes.string,
    value   : PropTypes.string,
    type    : PropTypes.string,
    rows    : PropTypes.number,
    onChange: PropTypes.func,
  }

  update = ({ target: { value } }) => {
    const { name, onChange } = this.props

    return onChange !== undefined && name !== undefined
      ? onChange({ [name]: value })
      : onChange(value)
  }

  render() {
    const { value, type, rows, mode = '' } = this.props

    return (
      type === 'textarea' ?
      <textarea
        rows={rows}
        className={css(styles.TextInput, styles.textarea, ...mode.split(' ').map(m => styles[m]))}
        onChange={this.update}
        value={value}
      />
        :
      <input
        className={css(styles.TextInput, ...mode.split(' ').map(m => styles[m]))}
        value={value}
        type={type}
        onChange={this.update}
      />
    )
  }
}

export default TextInput

const styles = StyleSheet.create({
  TextInput: {
    ...font({ size: 18 }),
    padding     : 5,
    marginBottom: 20,
    marginTop   : 5,
    border      : '1px solid #dadada',
  },

  block: {
    display: 'block',
  },

  textarea: {
    width   : '100%',
    maxWidth: '100%',
    minWidth: '100%',
    border  : '1px solid #aaa',
  }
})
