import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { color, font, touchable } from '../config/styles'

class Button extends Component {
  static propTypes = {
    caption : PropTypes.string,
    disabled: PropTypes.bool,
    mode    : PropTypes.string,
    onPress : PropTypes.func,
  }

  render() {
    const { caption, onPress, disabled, mode = '' } = this.props

    return (
      <div
        onClick={onPress}
        className={css(
          styles.Button,
          ...mode.split(' ').map(m => styles[m]),
          (disabled ? styles.disabled : null)
        )}
      >
        {caption}
      </div>
    )
  }
}

export default Button

const styles = StyleSheet.create({
  Button: {
    ...font(),
    ...touchable,
    display       : 'inline-block',
    padding       : '4px 10px',
    color         : color.active,
    borderRadius  : 4,
    letterSpacing : 0.7,
    textDecoration: 'none',
  },

  disabled: {
    opacity      : .2,
    pointerEvents: 'none',
  },

  expanded: {
    width: '100%',
  },

  primary: {
    border     : '1px solid',
    borderColor: color.active,
    background : color.active,
    color      : 'white',
  },

  larger: {
    fontSize: 18
  }
})
