import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { connect } from 'react-redux'
import Button from './Button'
import Col from './Col'
import Row from './Row'
import { confirmAction, dismissAction } from '../modules/root/store/actions'
import Text from './Text'

class ConfirmationDialog extends Component {
  static propTypes = {
    caption: PropTypes.string,
    onYes  : PropTypes.func,
    onNo   : PropTypes.func,
  }

  render() {
    const {
      confirmAction,
      dismissAction,
      caption = 'Are you sure?',
      onYes,
      onNo,
    } = this.props

    return (
      <div className={css(styles.ConfirmationDialog)}>
        <Text
          mode='block largest gapBottom'
          value={caption}
        />
        <Row>
          <Col>
            <Button
              mode='primary larger'
              onPress={onYes || confirmAction}
              caption='YES'
            />
          </Col>
          <Col>
            <Button
              mode='primary larger'
              onPress={onNo || dismissAction}
              caption='NO'
            />
          </Col>
        </Row>
      </div>
    )
  }
}

export default connect(
  null,
  {
    confirmAction,
    dismissAction,
  }
)(ConfirmationDialog)

const styles = StyleSheet.create({
  ConfirmationDialog: {
    minWidth    : 300,
    padding     : 20,
    background  : 'white',
    borderRadius: 4,
  },
})
