import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { screen } from '../config/styles'

class Row extends Component {
  static propTypes = {
    mode: PropTypes.string
  }

  render() {
    const { mode = '', children } = this.props

    return (
      <div className={css(styles.Row, ...mode.split(' ').map(m => styles[m]))}>
        {children}
      </div>
    )
  }
}

export default Row

const styles = StyleSheet.create({
  Row: {
    display       : 'flex',
    flexDirection : 'row',
    justifyContent: 'space-between',
    [screen.phone]: {
      flexDirection: 'column',
    },
  },

  wrap: {
    flexWrap: 'wrap'
  },

  start: {
    justifyContent: 'flex-start',
  }
})

