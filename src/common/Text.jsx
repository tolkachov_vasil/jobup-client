import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { color, font, size } from '../config/styles'

class TextInput extends Component {
  static propTypes = {
    className: PropTypes.string,
    mode     : PropTypes.string,
    value    : PropTypes.string,
    type     : PropTypes.string,
  }

  render() {
    const { children, value, className = '', mode = '' } = this.props

    return (
      <span className={css(styles.Text, ...mode.split(' ').map(m => styles[m])) + ` ${className }`}>
        {value || children}
      </span>
    )
  }
}

export default TextInput

const styles = StyleSheet.create({
  Text: {
    ...font()
  },

  block: {
    display: 'block'
  },

  center: {
    textAlign: 'center'
  },

  gray: {
    color: color.gray
  },

  gapBottom: {
    marginBottom: 10,
  },

  gapTop: {
    marginTop: 10,
  },

  spaced: {
    letterSpacing: 1.5,
  },

  smaller: {
    fontSize: size.font.smaller
  },

  larger: {
    fontSize: size.font.larger
  },

  largest: {
    fontSize: size.font.largest
  },
})
