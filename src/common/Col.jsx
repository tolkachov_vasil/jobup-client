import { css, StyleSheet } from 'aphrodite/no-important'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { screen } from '../config/styles'

class Col extends Component {
  static propTypes = {
    mode: PropTypes.string
  }

  render() {
    const { mode='',children } = this.props

    return (
      <div className={css(styles.Col, ...mode.split(' ').map(m => styles[m]))}>
        {children}
      </div>
    )
  }
}

export default Col

const styles = StyleSheet.create({
  Col   : {
    [screen.phone]: {
      textAlign: 'center',
    },
  },
  larger: {
    flexGrow: 0.5
  },

  right: {
    textAlign: 'right'
  },

  padded: {
    padding: 20
  },
})

