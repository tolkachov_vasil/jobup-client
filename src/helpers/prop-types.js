import PropTypes from 'prop-types'

export const ActionType = PropTypes.func.isRequired

export const IdType = PropTypes.string

export const LocationType = PropTypes.shape({
  lat: PropTypes.number,
  lng: PropTypes.number,
})

export const TaskType = PropTypes.shape({
  id         : IdType,
  address    : PropTypes.string,
  service    : PropTypes.string,
  task       : PropTypes.string,
  lat        : PropTypes.number,
  lng        : PropTypes.number,
  description: PropTypes.string,
  createdAt  : PropTypes.number,
})

export const UserType = PropTypes.shape({
  id       : IdType,
  email    : PropTypes.string,
  name     : PropTypes.string,
  createdAt: PropTypes.number,
})
