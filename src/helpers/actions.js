export const createAction = (type, ...keys) => (...args) => {
  const action = { type }
  keys.forEach((_, i) => action[keys[i]] = args[i])
  return action
}
