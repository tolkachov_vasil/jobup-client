import * as React from 'react'
import { put, race, take } from 'redux-saga/effects'
import ConfirmationDialog from '../common/ConfirmationDialog'
import { CONFIRM_ACTION, DISMISS_ACTION, showModal, } from '../modules/root/store/actions'

export function* confirm(caption) {
  yield put(showModal(<ConfirmationDialog caption={caption} />))

  const { yes } = yield race({
    yes: take(CONFIRM_ACTION),
    no : take(DISMISS_ACTION),
  })

  return !!yes
}
