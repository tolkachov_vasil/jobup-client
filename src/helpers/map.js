export const getMyLocation = () =>
  window.navigator.geolocation
    ? new Promise(resolve =>
      navigator.geolocation.getCurrentPosition(position =>
        resolve({
          lat: position.coords.latitude,
          lng: position.coords.longitude
        })))
    : { lat: 53.865, lng: 27.650 }

