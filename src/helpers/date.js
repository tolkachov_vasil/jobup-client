import moment from 'moment/moment'

const FORMAT = ', MMM DD, HH:mm'

moment.updateLocale('en', {
  calendar: {
    lastDay : '[Yesterday]',
    sameDay : '[Today]',
    nextDay : '[Tomorrow]',
    lastWeek: 'dddd',
    sameElse: 'dddd',
  }
})

export const formattedDate = dateString => {
  const date = moment(dateString)

  return date.calendar() + date.format(FORMAT)
}

export const orderByDate = (a, b) =>
  moment.utc(a).diff(moment.utc(b))
