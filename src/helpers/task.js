export const taskTitle = ({ service = '', task = '', description = '' }) =>
  service
    ? ('I need a ' + service.toLowerCase())
      + (task ? ' to ' + task.toLowerCase() : '...')
      + (description ? ', ' + description : '.')
    : ''

export const taskDTO = ({ lat, lng, address, service, task, description }) => ({
  lat, lng, address, service, task, description
})
