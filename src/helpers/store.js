import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducers from '../store/reducer'
import sagas from '../store/saga'

export function configureStore(context = {}) {
  const sagaMiddleware = createSagaMiddleware()
  const rootReducer = combineReducers(reducers)

  const store = compose(
    applyMiddleware(sagaMiddleware),
    window.devToolsExtension
      ? window.devToolsExtension()
      : f => f
  )(createStore)(rootReducer)

  sagaMiddleware.run(sagas, context)

  return store
}
