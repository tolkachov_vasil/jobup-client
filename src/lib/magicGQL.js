import * as plural from 'plural'
import { call, put, takeEvery } from 'redux-saga/effects'

export const MAGIC_REQUEST = 'MAGIC_REQUEST'
export const MAGIC_UPDATE = 'MAGIC_UPDATE'

export const magicRequest = (operation, entity, data, next) =>
  ({ type: MAGIC_REQUEST, operation, entity, data, next })

export const magicUpdate = (operation, field, value) =>
  ({ type: MAGIC_UPDATE, operation, field, value })

export const createMagicReducer = initial =>
  (state, action) =>
    magicReducer(state || initial, action)

export function* magicSaga(context, { query, mutation }) {
  yield takeEvery(MAGIC_REQUEST, request, context, query, mutation)
}

function* request({ apollo }, query, mutation, { operation, entity, data: variables = {}, next }) {
  const { data, errors, error } = operation === 'get'
    ? yield call(apollo.query, { query: query[operation][entity], variables })
    : yield call(apollo.mutate, { mutation: mutation[operation][entity], variables })

  yield (errors || error)
    ? put(magicUpdate('set', `${entity}_errors`, errors || error))
    : put(afterRequest[operation](operation, entity, data, variables))

  next && next()
}

const afterRequest = {

  get: (operation, entity, data) =>
    magicUpdate('set', entity, data[entity]),

  delete: (operation, entity, data, variables) =>
    magicUpdate('delete', plural(entity), variables),

  create: (operation, entity, data) =>
    magicUpdate('create', plural(entity), data[Object.keys(data)[0]]),

  update: (operation, entity, data) =>
    magicUpdate('update', plural(entity), data[Object.keys(data)[0]])
}

const magicReducer = (state, action) =>
  action.type === MAGIC_UPDATE
    ? magicReduce(state, action)
    : state

function magicReduce(state, { operation, field, value }) {
  switch(operation) {

  case 'set':
    return {
      ...state,
      [field]: value
    }
  case 'merge':
    return {
      ...state,
      [field]: { ...state[field], ...value }
    }
  case 'delete':
    return {
      ...state,
      [field]: state[field].filter(e => e.id !== value.id)
    }
  case 'create':
    return {
      ...state,
      [field]: [...state[field], value]
    }
  case 'update':
    return {
      ...state,
      [field]:
        [
          ...state[field].filter(e => e.id !== value.id),
          value
        ]
    }
  default:
    return state
  }
}

